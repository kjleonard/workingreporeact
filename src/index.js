/*
 * Copyright (c)  20 August 2018. PTW Services
 *
 * All rights are reserved.  If you would like to modify files, feel free to do so, however this copyright must remain prominently displayed on the top of the file.
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import YTSearch from 'youtube-api-search';
import VideoDetail from './components/video_detail';
import _ from 'lodash';


const API_KEY = 'AIzaSyBK1Hb_fRXTfHifWtkH___q-HVJyTiA3sU';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            selectedVideo: null,
            videos: []
        };

        this.videoSearch('cats')
    }
    videoSearch(term){
        YTSearch({key: API_KEY, term: term}, (videos) => {
            console.log(videos);
            this.setState({
                videos : videos,
                selectedVideo : videos[0]}
            )
        });
    }
    render(){

        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);

        return(
        <div>
            <SearchBar onSearchTermChange={videoSearch} />
            <VideoDetail video={this.state.selectedVideo}/>
            <VideoList
                onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                videos={this.state.videos} />
        </div>

        )
    }
};
ReactDOM.render(<App />, document.querySelector('.container'));
