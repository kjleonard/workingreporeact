/*
 * Copyright (c)  20 August 2018. PTW Services
 *
 * All rights are reserved.  If you would like to modify files, feel free to do so, however this copyright must remain prominently displayed on the top of the file.
 */

import React from 'react';

const VideoListItem = ({video, onVideoSelect}) => {

    const imageURL = video.snippet.thumbnails.default.url;
    const title = video.snippet.title;

    return (
        <li className="list-group-item" onClick={() => onVideoSelect(video)}>
        <div className="video-list media">
            <div className="media-left">
                <img className="media-object" src={imageURL}/>
            </div>
            <div className="media-body">
                <div className="media-heading">{title}</div>
            </div>
        </div>
        </li>
       )
};
export default VideoListItem;