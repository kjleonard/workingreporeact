/*
 * Copyright (c)  20 August 2018. PTW Services
 *
 * All rights are reserved.  If you would like to modify files, feel free to do so, however this copyright must remain prominently displayed on the top of the file.
 */

import React from 'react';
import VideoListItem from './video_list_item';

const VideoList = (props) =>{
    const videoItems = props.videos.map((video) => {
        return <VideoListItem
            onVideoSelect={props.onVideoSelect}
            key={video.etag}
            video={video} />
    });

  return (
      <ul className="col-md-4 list-group">
          {videoItems}
      </ul>
  );
};

export default VideoList;