/*
 * Copyright (c)  20 August 2018. PTW Services
 *
 * All rights are reserved.  If you would like to modify files, feel free to do so, however this copyright must remain prominently displayed on the top of the file.
 */
import React from 'react';

class SearchBar extends React.Component{
    constructor(props){
        super(props);

        this.state = {term: ''};
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    placeholder={this.state.term === '' ? 'Search Youtube' : this.state.term}
                    value={this.state.term}
                    onChange={event =>this.onInputChange(event.target.value)}
                />
            </div>
        );
    }

    onInputChange(term){
        this.setState({term});
        this.props.onSearchTermChange(term);
    }

}

export default SearchBar;
