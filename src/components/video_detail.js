/*
 * Copyright (c)  20 August 2018. PTW Services
 *
 * All rights are reserved.  If you would like to modify files, feel free to do so, however this copyright must remain prominently displayed on the top of the file.
 */

import React from 'react'

const VideoDetail = ({video}) => {
    if(!video){
        return <div>Loading...</div>;
    }

    const videoID = video.id.videoId;
    const url = `https://www.youtube.com/embed/${videoID}`;

    return (
        <div className="video-detail com-md-8">
            <div className="embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" src={url}></iframe>
            </div>

            <div className="details">
                <div>{video.snippet.title}</div>
                <div>{video.snippet.description}</div>
            </div>
        </div>
    );
};

export default VideoDetail;
